//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "uLWT_newUser.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm2 *Form2;
//---------------------------------------------------------------------------
__fastcall TForm2::TForm2(TComponent* Owner)
        : TForm(Owner)
{
        
}
//---------------------------------------------------------------------------
void __fastcall TForm2::Button1Click(TObject *Sender)
{
        if(Edit1->Text.IsEmpty()) return;
        User u;
        u.on=false;
        u.gateway=(ComboBox1->ItemIndex<0)?0:ComboBox1->ItemIndex;
        u.name=Edit1->Text;
        Form1->AddUser(u);

        Hide();     
}
//---------------------------------------------------------------------------
void __fastcall TForm2::Button2Click(TObject *Sender)
{
        Hide();        
}
//---------------------------------------------------------------------------
void __fastcall TForm2::Hide()
{
        Edit1->Text="";
        TForm::Hide();
}
void __fastcall TForm2::SetLanguage(int nr)
{
        Button1->Caption=F2But1[nr];
        Button2->Caption=F2But2[nr];
}
void __fastcall TForm2::FormActivate(TObject *Sender)
{
        SetLanguage(Form1->lng);
}
//---------------------------------------------------------------------------

void __fastcall TForm2::FormKeyPress(TObject *Sender, char &Key)
{
        if((Key==VK_RETURN)&&(!Edit1->Text.IsEmpty())) Button1Click(0);
}
//---------------------------------------------------------------------------

