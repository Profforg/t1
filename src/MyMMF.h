//---------------------------------------------------------------------------

#ifndef MyMMFH
#define MyMMFH
//---------------------------------------------------------------------------

#include <vcl.h>

#include <string>
const int max=100;

template<class T>
inline bool __fastcall WriteToMMF(char* name, T data)
{
        HANDLE mmf = CreateFileMapping(INVALID_HANDLE_VALUE,NULL,
                                       PAGE_READWRITE,0, sizeof(T),name);
        if(!mmf) return false;
        LPVOID mem = MapViewOfFile(mmf, FILE_MAP_WRITE,0,0,sizeof(T));
        if(!mem) return false;
        CopyMemory(mem, &data, sizeof(T));
        
        return true;

}
template<class T>
inline bool __fastcall ReadFromMMF(char* name, T& data)
{
        HANDLE mmf = OpenFileMapping(FILE_MAP_READ,false,name);
        if(!mmf) return false;
        LPVOID mem = MapViewOfFile(mmf, FILE_MAP_READ,0,0,sizeof(T));
        if(!mem) return false;
        CopyMemory(&data,mem , sizeof(T));

        return true;
}
#endif
