//---------------------------------------------------------------------------

#ifndef uLWT_newUserH
#define uLWT_newUserH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "gHeader.h"
#include "uLwt.h"

//---------------------------------------------------------------------------
class TForm2 : public TForm
{
__published:	// Von der IDE verwaltete Komponenten
        TComboBox *ComboBox1;
        TLabel *Label1;
        TLabel *Label2;
        TEdit *Edit1;
        TButton *Button1;
        TButton *Button2;
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall FormActivate(TObject *Sender);
        void __fastcall FormKeyPress(TObject *Sender, char &Key);
private:	// Anwender-Deklarationen
        void virtual __fastcall Hide();

public:		// Anwender-Deklarationen
        __fastcall TForm2(TComponent* Owner);
        void __fastcall SetLanguage(int nr);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm2 *Form2;
//---------------------------------------------------------------------------
#endif
