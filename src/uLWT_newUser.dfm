object Form2: TForm2
  Left = 497
  Top = 489
  Width = 237
  Height = 161
  Caption = 'LWT: User hinzuf'#252'gen'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnActivate = FormActivate
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 40
    Width = 45
    Height = 13
    Caption = 'Gateway:'
  end
  object Label2: TLabel
    Left = 8
    Top = 16
    Width = 74
    Height = 13
    Caption = 'Account-Name:'
  end
  object ComboBox1: TComboBox
    Left = 64
    Top = 40
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 0
    Text = 'Europe (Northrend)'
    Items.Strings = (
      'Europe (Northrend)'
      'Asia (Kalimdor)'
      'US-West (Lordaeron)'
      'US-East (Azeroth) ')
  end
  object Edit1: TEdit
    Left = 88
    Top = 16
    Width = 121
    Height = 21
    MaxLength = 20
    TabOrder = 1
  end
  object Button1: TButton
    Left = 8
    Top = 72
    Width = 201
    Height = 25
    Caption = 'Hinzuf'#252'gen'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 8
    Top = 96
    Width = 201
    Height = 25
    Caption = 'Abbrechen'
    TabOrder = 3
    OnClick = Button2Click
  end
end
