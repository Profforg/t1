//---------------------------------------------------------------------------

#ifndef uLWT_newHkH
#define uLWT_newHkH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "uLWT.h"
#include "gHeader.h"
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TForm5 : public TForm
{
__published:	// Von der IDE verwaltete Komponenten
        TButton *Button1;
        TButton *Button2;
        THotKey *HotKey1;
        TLabel *Label1;
        TLabel *Label2;
        TRichEdit *RichEdit1;
        void __fastcall FormActivate(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall RichEdit1KeyPress(TObject *Sender, char &Key);
private:	// Anwender-Deklarationen
public:		// Anwender-Deklarationen
        __fastcall TForm5(TComponent* Owner);
        void __fastcall SetLangugage(int nr);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm5 *Form5;
//---------------------------------------------------------------------------
#endif
