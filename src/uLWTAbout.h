//---------------------------------------------------------------------------

#ifndef uLWTAboutH
#define uLWTAboutH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TForm4 : public TForm
{
__published:	// Von der IDE verwaltete Komponenten
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label4;
        TLabel *Label6;
        TLabel *Label5;
        TLabel *Label7;
        TLabel *Label8;
        TLabel *Label9;
        TButton *Button1;
        TLabel *Label10;
        TLabel *Label11;
        TLabel *Label13;
        TLabel *Label12;
        TLabel *Label14;
        TLabel *Label16;
        TLabel *Label15;
        void __fastcall Label5Click(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall Label8Click(TObject *Sender);
        void __fastcall Label11Click(TObject *Sender);
        void __fastcall Label12Click(TObject *Sender);
        void __fastcall Label4Click(TObject *Sender);
        void __fastcall Label15Click(TObject *Sender);
private:	// Anwender-Deklarationen
public:		// Anwender-Deklarationen
        __fastcall TForm4(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm4 *Form4;
//---------------------------------------------------------------------------
#endif
