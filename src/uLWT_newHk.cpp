//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "uLWT_newHk.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm5 *Form5;
//---------------------------------------------------------------------------
__fastcall TForm5::TForm5(TComponent* Owner)
        : TForm(Owner)
{


}
//---------------------------------------------------------------------------
void __fastcall TForm5::SetLangugage(int nr)
{
        Caption="LWT - "+But21[nr];
        Button1->Caption=F2But2[nr];
        Button2->Caption=F2But1[nr];
}
void __fastcall TForm5::FormActivate(TObject *Sender)
{
        this->SetLangugage(Form1->lng);
}
//---------------------------------------------------------------------------

void __fastcall TForm5::Button2Click(TObject *Sender)
{
        if(RichEdit1->Text.IsEmpty()) return;
        TextHotKey th;
        th.hk=HotKey1->HotKey;
        th.text=RichEdit1->Text;
        this->Hide();
        Form1->AddHotKey(th);

}
//---------------------------------------------------------------------------

void __fastcall TForm5::Button1Click(TObject *Sender)
{
        Hide();        
}
//---------------------------------------------------------------------------


void __fastcall TForm5::RichEdit1KeyPress(TObject *Sender, char &Key)
{
        if(RichEdit1->Text.Length()>99)
                Key=0;
}
//---------------------------------------------------------------------------

