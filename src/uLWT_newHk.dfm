object Form5: TForm5
  Left = 265
  Top = 137
  Width = 228
  Height = 210
  Caption = 'LWT - Neuer Hotkey'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 8
    Width = 37
    Height = 13
    Caption = 'Hotkey:'
  end
  object Label2: TLabel
    Left = 88
    Top = 32
    Width = 21
    Height = 13
    Caption = 'Text'
  end
  object Button1: TButton
    Left = 8
    Top = 152
    Width = 209
    Height = 17
    Caption = 'cancel'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 8
    Top = 128
    Width = 209
    Height = 17
    Caption = 'ok'
    TabOrder = 1
    OnClick = Button2Click
  end
  object HotKey1: THotKey
    Left = 64
    Top = 8
    Width = 145
    Height = 19
    HotKey = 32833
    InvalidKeys = [hcNone, hcShift]
    Modifiers = [hkAlt]
    TabOrder = 2
  end
  object RichEdit1: TRichEdit
    Left = 24
    Top = 48
    Width = 177
    Height = 73
    TabOrder = 3
    OnKeyPress = RichEdit1KeyPress
  end
end
