//---------------------------------------------------------------------------

#ifndef uLwtH
#define uLwtH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <AutoStart.h>
#include <ComCtrls.hpp>
#include <CoolTrayIcon.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <jpeg.hpp>
#include <Menus.hpp>
#include <ScktComp.hpp>
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdIcmpClient.hpp>
#include <IdRawBase.hpp>
#include <IdRawClient.hpp>
#include "cdiroutl.h"
#include <Grids.hpp>
#include <Outline.hpp>
#include <Dialogs.hpp>
#include <IdTCPClient.hpp>
#include <IdTCPConnection.hpp>
#include <ValEdit.hpp>
#include <vector>
#include <HotKeyClass.h>
#include "gHeader.h"
#include "disKeys.h"
#include "uLWT_newUser.h"
#include "uLWTProfile.h"
#include "uLWTAbout.h"
#include "MyMMF.h"

//---------------------------------------------------------------------------
// KONSTANTEN
const AnsiString KeyName="Lescos Warcraft Toolkit",
      Titel="Lescos Warcraft Toolkit 0.7 beta",on="Aktiv",off="Inaktiv",
      off_b="Aktivieren",on_b="Deaktivieren",fn="LWT.dat",ul_fn="LWT",
      EXT_UL=".ul",EXT_CNT=".ucnt",libname="LWTlib_v2.dll";
const TColor MyRed=0x000C0CDA,
      MyGreen=clGreen;

const AnsiString hosts[4]={"europe.battle.net","asia.battle.net",
                             "uswest.battle.net","useast.battle.net"},

                 gwNames[4]={"Europe(Northrend)","Asia(Kalimdor)",
                             "US-West(Lodaeron)","US-East(Azeroth)"};

// ENDE KONSTANTEN
typedef void __stdcall TInstall(disKeys k);
typedef void __stdcall TUnInstall();

void __stdcall __declspec(dllexport) Install(disKeys k);
void __stdcall __declspec(dllexport) UnInstall();

//***Struct zur Speicherung***
struct LWTData
{
        
        bool cbs[5]; //4,1,2,5,6
        int lng;
        int hks[10]; // 0-1: energie!!! 2-7: inventar!!! 8-9 features an/aus

};
//****************************
struct UserStatus
{
        bool on;
        AnsiString ort;
};


class TForm1 : public TForm
{
__published:	// Von der IDE verwaltete Komponenten
        TButton *Button1;
        TButton *Button6;
        TImage *Image1;
        TPopupMenu *PopupMenu1;
        TMenuItem *Anzeigen1;
        TMenuItem *Beenden1;
        TButton *Button2;
        TButton *Button3;
        TClientSocket *pingSock;
        TOpenDialog *OpenDialog1;
        TIdTCPClient *lsock;
        TTimer *Timer2;
        TTimer *killTimer;
        TIdTCPClient *IdTCPClient1;
        TIdTCPClient *IdTCPClient2;
        TIdTCPClient *IdTCPClient3;
        TTimer *msgTimer;
        TComboBox *ComboBox2;
        TLabel *Label27;
        TButton *Button17;
        TImage *Image2;
        TImage *Image3;
        TImage *Image4;
        TPageControl *PageControl1;
        TTabSheet *TabSheet1;
        TLabel *Label5;
        TLabel *Label4;
        TLabel *Label3;
        TLabel *Label2;
        TLabel *Label24;
        TLabel *Label25;
        TLabel *Label28;
        TLabel *Label29;
        TImage *Image5;
        TImage *Image6;
        TImage *Image7;
        TButton *Button8;
        TButton *Button7;
        TButton *Button10;
        TButton *Button13;
        THotKey *HotKey9;
        THotKey *HotKey10;
        TButton *Button18;
        TTabSheet *TabSheet2;
        TGroupBox *GroupBox1;
        TCheckBox *CheckBox1;
        TCheckBox *CheckBox2;
        TCheckBox *CheckBox3;
        TCheckBox *CheckBox5;
        TGroupBox *GroupBox2;
        TLabel *Label6;
        TLabel *Label7;
        TCheckBox *CheckBox4;
        THotKey *HotKey1;
        THotKey *HotKey2;
        TButton *Button9;
        TTabSheet *TabSheet3;
        TLabel *Label8;
        TLabel *Label9;
        TLabel *Label10;
        TLabel *Label11;
        TLabel *Label12;
        TLabel *Label13;
        TGroupBox *GroupBox3;
        TPanel *Panel1;
        TPanel *Panel2;
        TPanel *Panel3;
        TPanel *Panel4;
        TPanel *Panel5;
        TPanel *Panel6;
        TButton *Button4;
        THotKey *HotKey3;
        THotKey *HotKey4;
        THotKey *HotKey5;
        THotKey *HotKey6;
        THotKey *HotKey7;
        THotKey *HotKey8;
        TTabSheet *TabSheet4;
        TGroupBox *GroupBox4;
        TButton *Button11;
        TComboBox *ComboBox1;
        TEdit *Edit1;
        TGroupBox *GroupBox5;
        TLabel *Label23;
        TListBox *ListBox1;
        TGroupBox *GroupBox6;
        TLabel *Label14;
        TLabel *Label15;
        TLabel *Label16;
        TLabel *Label17;
        TLabel *Label18;
        TLabel *Label19;
        TLabel *Label20;
        TRichEdit *RichEdit1;
        TButton *Button12;
        TButton *Button5;
        TButton *Button16;
        TTabSheet *TabSheet5;
        TLabel *Label22;
        TGroupBox *GroupBox7;
        TLabel *Label21;
        TEdit *Edit2;
        TButton *Button14;
        TButton *Button15;
        TTimer *minTimer;
        TMenuItem *StartW3ROC1;
        TMenuItem *StartW3TFT1;
        TCheckBox *CheckBox6;
        void __fastcall Button6Click(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall Anzeigen1Click(TObject *Sender);
        void __fastcall Beenden1Click(TObject *Sender);
        void __fastcall Button5Click(TObject *Sender);
        void __fastcall Button9Click(TObject *Sender);
        void __fastcall Button7Click(TObject *Sender);
        void __fastcall HotKey1Change(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall Button4Click(TObject *Sender);
        void __fastcall Button8Click(TObject *Sender);
        void __fastcall Button10Click(TObject *Sender);
        void __fastcall pingSockError(TObject *Sender,
          TCustomWinSocket *Socket, TErrorEvent ErrorEvent,
          int &ErrorCode);
        void __fastcall Button11Click(TObject *Sender);
        void __fastcall pingSockRead(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
        void __fastcall ListBox1Click(TObject *Sender);
        void __fastcall Button12Click(TObject *Sender);
        void __fastcall Button15Click(TObject *Sender);
        void __fastcall Button14Click(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall OnTrayMouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
        void __fastcall Button13Click(TObject *Sender);
        void __fastcall ListBox1DblClick(TObject *Sender);
        void __fastcall ListBox1DrawItem(TWinControl *Control, int Index,
          TRect &Rect, TOwnerDrawState State);
        void __fastcall Button16Click(TObject *Sender);
        void __fastcall lsockConnected(TObject *Sender);
        void __fastcall Timer2Timer(TObject *Sender);
        void __fastcall killTimerTimer(TObject *Sender);
        void __fastcall msgTimerTimer(TObject *Sender);
        void __fastcall ComboBox2Change(TObject *Sender);
        void __fastcall HotKey10Change(TObject *Sender);
        void __fastcall Button17Click(TObject *Sender);
        void __fastcall Button18Click(TObject *Sender);
        bool __fastcall FormHelp(WORD Command, int Data, bool &CallHelp);
        void __fastcall minTimerTimer(TObject *Sender);
        void __fastcall StartW3ROC1Click(TObject *Sender);
        void __fastcall StartW3TFT1Click(TObject *Sender);
        void __fastcall ComboBox2KeyPress(TObject *Sender, char &Key);


private:	// Anwender-Deklarationen
        bool auto_en,tast_on, inv_hks_on,uon,list_on,dll_loaded;
        bool hk_on[2];
        int pingDur,g_gw;
        DWORD d; // GetTickCount() bei Ping
        //**************** vektoren *********************
        std::vector<bool*> features;
        std::vector<TLabel*> act_labs;
        std::vector<TButton*> act_buts, act_buts2;
        std::vector<UserStatus> uss;        
        std::vector<THotKey*> inv_hks;
        std::vector<User> users;
        std::vector<TIdTCPClient*> lsocks;
        std::vector<TImage*> leds;

        //***********************************************
        THotKey * hks[2], * fhks[2];
        Cooltrayicon::TCoolTrayIcon * tray;
        void __fastcall Toggle(int nr, bool val);
        void __fastcall OnTrayDblClick(TObject *Sender);
        void __fastcall OnTrayClick(TObject *Sender);
        void MESSAGE __fastcall HotKey(TMessage& msg);
        BEGIN_MESSAGE_MAP
                VCL_MESSAGE_HANDLER(WM_HOTKEY, TMessage, HotKey);
        END_MESSAGE_MAP(TForm);
        HANDLE hThread;
        HMODULE lib;
        TInstall* Install;
        TUnInstall* UnInstall;
        AnsiString __fastcall getW3Exe(bool tft);
        //********Funktionen zum Laden/Speichern***********
        bool __fastcall SaveToFile(AnsiString fn);
        bool __fastcall LoadFromFile(AnsiString fn);
        //*************************************************
        //********Funktionen zur Userlist******************
        UserStatus __fastcall getUserStatus(int nr);
        void __fastcall SaveUserList(AnsiString fn);
        void __fastcall LoadUserList(AnsiString fn);
        UserStatus __fastcall IsUserOn(AnsiString resp,int nr);
        void __fastcall ShowUserStatus(int nr, bool labs=true);
        void __fastcall RefreshList(bool msg=false);
        void __fastcall ResetList();
        void __fastcall ProcessUserStatus(UserStatus us,int nr,bool list=false);
        //*************************************************
        void __fastcall SetLanguage(int nr);
        void __fastcall BlendForm();
public:		// Anwender-Deklarationen
        friend DWORD WINAPI tWrapper(LPVOID);
        friend DWORD WINAPI Blend(LPVOID);
        int lng;
         __fastcall TForm1(TComponent* Owner);
        void __fastcall AddUser(User u);
        void __fastcall DeleteUser(int nr);

};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;

//---------------------------------------------------------------------------
#endif
