//---------------------------------------------------------------------------

#include <windows.h>
//---------------------------------------------------------------------------
//   Wichtiger Hinweis zur DLL-Speicherverwaltung, falls die DLL die statische
//   Version der Laufzeitbibliothek (RTL) verwendet:
//
//   Wenn die DLL Funktionen exportiert, die String-Objekte (oder Strukturen/
//   Klassen, die verschachtelte Strings enthalten) als Parameter oder Funktionsergebnisse �bergibt,
//   mu� die Bibliothek MEMMGR.LIB im DLL-Projekt und anderen Projekten,
//   die die DLL verwenden, vorhanden sein. Sie ben�tigen MEMMGR.LIB auch dann,
//   wenn andere Projekte, die die DLL verwenden, new- oder delete-Operationen
//   auf Klassen anwenden, die nicht von TObject abgeleitet sind und die aus der DLL exportiert
//   werden. Durch das Hinzuf�gen von MEMMGR.LIB wird die DLL und deren aufrufende EXEs
//   angewiesen, BORLNDMM.DLL als Speicherverwaltung zu benutzen. In diesem Fall
//   sollte die Datei BORLNDMM.DLL zusammen mit der DLL weitergegeben werden.
//
//   Um die Verwendung von BORLNDMM.DLL, zu vermeiden, sollten String-Informationen als "char *" oder
//   ShortString-Parameter weitergegeben werden.
//
//   Falls die DLL die dynamische Version der RTL verwendet, m�ssen Sie
//   MEMMGR.LIB nicht explizit angeben.
//---------------------------------------------------------------------------

#pragma argsused
#include "disKeys.h"
#include <vcl.h>
#include <string>
#include "MyMMF.h"
char* MMF="LWTLIBMMF";
void __stdcall __declspec(dllexport) Install(disKeys k);
void __stdcall __declspec(dllexport) UnInstall();
HHOOK hhk1=0,hhk2=0;

HINSTANCE gInst;
LRESULT CALLBACK KBLLProc(int nCode,WPARAM wParam,LPARAM lParam);
int WINAPI DllEntryPoint(HINSTANCE hinst, unsigned long reason, void* lpReserved)
{
        gInst=hinst;
        return 1;
}
//---------------------------------------------------------------------------
void __stdcall __declspec(dllexport) Install(disKeys k)
{

        hhk1=SetWindowsHookEx(WH_KEYBOARD_LL,(HOOKPROC)KBLLProc,gInst,0);
        WriteToMMF<disKeys>(MMF, k);

}
LRESULT CALLBACK KBLLProc(int nCode,WPARAM wParam,LPARAM lParam)
{
        disKeys gdk;
        ReadFromMMF(MMF,gdk);
        bool kill=false;
        if(nCode<0)
        return CallNextHookEx(hhk1, nCode, wParam, lParam);
        KBDLLHOOKSTRUCT * key=(KBDLLHOOKSTRUCT*)lParam;
        if(gdk.alttab)
        {
                if(key->vkCode==VK_TAB)
                {
                        short alt=GetAsyncKeyState(VK_MENU);
                        if(alt >> 31) kill=true;
                }
        }
        if(gdk.win)
        {
                if((key->vkCode==VK_LWIN)||(key->vkCode==VK_RWIN))
                {
                        kill=true;
                }
        }
        if(gdk.altq)
        {
                if(key->vkCode==0x51)
                {
                        short alt=GetAsyncKeyState(VK_MENU);
                        if(alt >> 31) kill=true;
                }
                
        }
        if(gdk.altesc)
        {
                if(key->vkCode==VK_ESCAPE)
                {
                        short alt=GetAsyncKeyState(VK_MENU);
                        if(alt >> 31) kill=true;
                }
        }
        /* Special Keys:
                alt -> 0xa4
                shift -> 0xa0
        */
        /*char c[20];
        char * a=c;
        char * b=itoa(VK_MENU,a,16);  */
        //if(strcmp(b,"a0")==0)
        //MessageBox(0,b,"KEY",0);
        if(gdk.altshift)
        {
                if(key->vkCode==0xa0)
                {
                        short n=GetAsyncKeyState(0xa4);
                        if(n >> 31)
                        kill=true;
                }
                if(key->vkCode==0xa4)
                {
                        short n=GetAsyncKeyState(0xa0);
                        if(n >> 31)
                        kill=true;
                }
        }
        if(kill) return 1;
        return CallNextHookEx(hhk1, nCode, wParam, lParam);
}
void __stdcall __declspec(dllexport) UnInstall()
{
        UnhookWindowsHookEx(hhk1);
        UnhookWindowsHookEx(hhk2);
}
