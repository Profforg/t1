object Form4: TForm4
  Left = 186
  Top = 71
  Width = 273
  Height = 288
  Caption = 'LWT - About'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 101
    Height = 13
    Caption = 'Main programmer:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 32
    Width = 152
    Height = 13
    Caption = 'Homepage and Publishing:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 168
    Top = 32
    Width = 79
    Height = 13
    Cursor = crHandPoint
    Caption = 'Memphis O'#39'Brien'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsUnderline]
    ParentFont = False
    OnClick = Label4Click
  end
  object Label6: TLabel
    Left = 88
    Top = 56
    Width = 59
    Height = 13
    Caption = 'Big thx to:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 8
    Top = 72
    Width = 88
    Height = 13
    Cursor = crHandPoint
    Caption = 'www.inwarcraft.de'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsUnderline]
    ParentFont = False
    OnClick = Label5Click
  end
  object Label7: TLabel
    Left = 24
    Top = 88
    Width = 141
    Height = 13
    Caption = 'for hosting this in their filebase'
  end
  object Label8: TLabel
    Left = 8
    Top = 104
    Width = 125
    Height = 13
    Cursor = crHandPoint
    Caption = 'Inwarcraft.de Forum users:'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsUnderline]
    ParentColor = False
    ParentFont = False
    OnClick = Label8Click
  end
  object Label9: TLabel
    Left = 24
    Top = 120
    Width = 221
    Height = 13
    Caption = 'for much feedback and bug-reports and testing'
  end
  object Label10: TLabel
    Left = 16
    Top = 200
    Width = 36
    Height = 13
    Caption = 'Visit us:'
  end
  object Label11: TLabel
    Left = 56
    Top = 200
    Width = 165
    Height = 13
    Cursor = crHandPoint
    Caption = 'www.lesco.philipp-nico-krueger.de '
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsUnderline]
    ParentColor = False
    ParentFont = False
    OnClick = Label11Click
  end
  object Label13: TLabel
    Left = 24
    Top = 152
    Width = 75
    Height = 13
    Caption = 'for Beta-Testing'
  end
  object Label12: TLabel
    Left = 128
    Top = 136
    Width = 41
    Height = 13
    Cursor = crHandPoint
    Caption = 'Manwe_'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsUnderline]
    ParentColor = False
    ParentFont = False
    OnClick = Label12Click
  end
  object Label14: TLabel
    Left = 96
    Top = 136
    Width = 21
    Height = 13
    Caption = 'and '
  end
  object Label16: TLabel
    Left = 8
    Top = 136
    Width = 79
    Height = 13
    Cursor = crHandPoint
    Caption = 'Memphis O'#39'Brien'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsUnderline]
    ParentFont = False
    OnClick = Label4Click
  end
  object Label15: TLabel
    Left = 168
    Top = 8
    Width = 29
    Height = 13
    Cursor = crHandPoint
    Caption = 'Lesco'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsUnderline]
    ParentFont = False
    OnClick = Label15Click
  end
  object Button1: TButton
    Left = 8
    Top = 224
    Width = 241
    Height = 25
    Caption = 'Close'
    TabOrder = 0
    OnClick = Button1Click
  end
end
