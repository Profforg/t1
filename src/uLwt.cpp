//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "uLwt.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
        Caption=Titel;
        lng=1;
        leds.push_back(Image5);
        leds.push_back(Image6);
        leds.push_back(Image7);
        fhks[0]=HotKey9;
        fhks[1]=HotKey10;
        tray = new Cooltrayicon::TCoolTrayIcon(this);
        tray->Icon=(TIcon*)Image1->Picture;
        tray->IconVisible=true;
        tray->Hint=Titel;
        tray->OnDblClick=OnTrayDblClick;
        tray->PopupMenu=PopupMenu1;
        tray->OnMouseUp=OnTrayMouseUp;
        tray->MinimizeToTray=true;
        act_labs.push_back(Label4);
        act_labs.push_back(Label5);
        act_labs.push_back(Label25);
        act_buts.push_back(Button7);
        act_buts.push_back(Button8);
        act_buts.push_back(Button13);

        act_buts2.push_back(Button9);
        act_buts2.push_back(Button4);
        act_buts2.push_back(Button16);
        inv_hks.push_back(HotKey3);
        inv_hks.push_back(HotKey4);
        inv_hks.push_back(HotKey5);
        inv_hks.push_back(HotKey6);
        inv_hks.push_back(HotKey7);
        inv_hks.push_back(HotKey8);
        features.push_back(&tast_on);
        features.push_back(&inv_hks_on);
        features.push_back(&uon);
        lsocks.push_back(lsock);
        lsocks.push_back(IdTCPClient1);
        lsocks.push_back(IdTCPClient2);
        lsocks.push_back(IdTCPClient3);
        hks[0]=HotKey1;
        hks[1]=HotKey2;
        Toggle(-1, false);
        hk_on[0]=hk_on[1]=list_on=uon=tast_on=inv_hks_on=dll_loaded=false;
        //********Einstellungen laden************
        if(FileExists(fn))
        {
                LoadFromFile(fn);
        }
        LoadUserList(ul_fn);
        //***************************************
        Label21->Caption="Deine Warcraft 3 Registry Eintr�ge sind durch Windows-Reinstall falsch/nicht vorhanden?\nDann benutze dieses Men� um die Eintr�ge wiederherzustellen";
        if(ParamCount()==1)
        {
                if(ParamStr(1)=="-minimized")
                        minTimer->Enabled=true;
        }
        CreateThread(0,0,Blend,0,0,0);

        Application->ProcessMessages();
        SetLanguage(lng);
}

//---------------------------------------------------------------------------
void __fastcall TForm1::Button6Click(TObject *Sender)
{
        if(auto_en)
        {
                AutoStart::SetAutoStart(false, KeyName, "");
                auto_en=false;
                Button6->Caption=But6Act[lng];
                return;
        }
        if(!auto_en)
        {
                AutoStart::SetAutoStart(true, KeyName, "\""+Application->ExeName+"\" -minimized");
                auto_en=true;
                Button6->Caption=But6InAct[lng];
                return;
        }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
        tray->HideMainForm();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::OnTrayDblClick(TObject *Sender)
{
        tray->ShowMainForm();
        Application->BringToFront();
}
void __fastcall TForm1::Toggle(int nr, bool val)
{
        AnsiString capt=(val)?LabAct[lng]:LabInAct[lng];
        AnsiString capt_but=(val)?ButInAct[lng]:ButAct[lng];
        TColor col = (val)?MyGreen:MyRed;
        if(nr==-1)
        {
                for(unsigned int i=0;i<act_buts.size();++i)
                {
                        Toggle(i, val);
                }
                return;
        }
        //buttons
        
        if(nr>act_buts.size()-1) return;
        act_buts[nr]->Caption=capt_but;
        if(nr>act_labs.size()-1) return;
        //Labels
        act_buts2[nr]->Caption=capt_but;
        act_labs[nr]->Caption=capt;
        act_labs[nr]->Font->Color=col;
        //bilder
        leds[nr]->Picture=(val)?Image3->Picture:Image4->Picture;

        return;
}
void __fastcall TForm1::Anzeigen1Click(TObject *Sender)
{
        OnTrayDblClick(0);        
}
//---------------------------------------------------------------------------
void __fastcall TForm1::OnTrayClick(TObject *Sender)
{
        tray->PopupAtCursor();
}

void __fastcall TForm1::Beenden1Click(TObject *Sender)
{
        this->Close();        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button5Click(TObject *Sender)
{
        if(ListBox1->ItemIndex<0) return;
        DeleteUser(ListBox1->ItemIndex);
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

void __fastcall TForm1::Button9Click(TObject *Sender)
{
        Button7Click(0);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button7Click(TObject *Sender)
{
        if(!dll_loaded)
        {
                lib=LoadLibrary((ExtractFileDir(Application->ExeName)+"\\"+libname).c_str());
                if(lib==0)
                {
                        ShowMessage(libname+DllMissing[lng]);
                        this->Close();
                        return;
                }
                Install=(TInstall*)GetProcAddress(lib, "@Install$qqs7disKeys");
                UnInstall=(TUnInstall*)GetProcAddress(lib, "@UnInstall$qqsv");
                if((Install==0)||(UnInstall==0))
                {
                        ShowMessage(libname+DllMissing[lng]);
                        this->Close();
                }
                dll_loaded=true;
        }
        if(!tast_on)
        {
                if(CheckBox4->Checked)
                {
                        for(int i=0;i<2;++i)
                        {
                                HotKeyClass * hkc=new HotKeyClass(hks[i]);
                                KeyStruct ks;
                                hkc->getKeyStruct(ks);
                                if(!RegisterHotKey(Handle, i, ks.mods, ks.vk))
                                {

                                        AnsiString errText = "Hotkey: ";
                                        errText = errText+ks.vk;
                                        errText += " konnte nicht registriert werden\nEventuell schon von anderer Anwendung belegt";
                                        ShowMessage(errText);
                                }

                        }

                }
                disKeys k;
                k.altq=CheckBox1->Checked;
                k.win=CheckBox2->Checked;
                k.alttab=CheckBox3->Checked;
                k.altshift=CheckBox5->Checked;
                //ShowMessage(IntToStr(CheckBox2->Enabled)+IntToStr(k.altq)+IntToStr(k.alttab));
                k.altesc=CheckBox6->Checked;    
                (*Install)(k);


        }
        if(tast_on)
        {
                for(int i=0;i<2;++i)
                {
                        UnregisterHotKey(Handle, i);
                }
                (*UnInstall)();
        }
        for(int i=0;i<2;++i)
        {
                hks[i]->Enabled=tast_on;
        }
        CheckBox1->Enabled=tast_on;
        CheckBox2->Enabled=tast_on;
        CheckBox3->Enabled=tast_on;
        CheckBox4->Enabled=tast_on;
        CheckBox5->Enabled=tast_on;
        CheckBox6->Enabled=tast_on;
        tast_on=!tast_on;
        Toggle(0, tast_on);

}
//---------------------------------------------------------------------------

void MESSAGE __fastcall TForm1::HotKey(TMessage& msg)
{
        if(msg.WParam==0||msg.WParam==1)
        {
                int flag=(hk_on[msg.WParam])?KEYEVENTF_KEYUP:0;
                int vk=(msg.WParam)?221:219;
                keybd_event(vk, 0x45, flag,0);
                hk_on[msg.WParam] =!hk_on[msg.WParam];
                return;
        }
        if((msg.WParam<8)&&(msg.WParam>1))
        {
                int vks[6]={VK_NUMPAD7,VK_NUMPAD8,VK_NUMPAD4,
                            VK_NUMPAD5,VK_NUMPAD1,VK_NUMPAD2};
                keybd_event(vks[msg.WParam-2],0x45,0,0);
                keybd_event(vks[msg.WParam-2], 0x45, KEYEVENTF_KEYUP,0);
        }
        if(msg.WParam==8)
        {
                Button7Click(0);
        }
        if(msg.WParam==9)
        {
                Button8Click(0);
        }

        return;
}

void __fastcall TForm1::HotKey1Change(TObject *Sender)
{
        if(HotKey1->HotKey==HotKey2->HotKey)
        {
                ShowMessage(ErrHotkeyDefined[lng]);
                HotKey2->HotKey++;
        }
}
//---------------------------------------------------------------------------

AnsiString __fastcall TForm1::getW3Exe(bool tft)
{
        TRegistry * reg = new TRegistry();
        reg->RootKey=HKEY_CURRENT_USER;
        if(!reg->OpenKey("\\Software\\Blizzard Entertainment\\Warcraft III\\",false))
        {
                ShowMessage(ErrDirNotFound[lng]);
                return "err";
        }
        return (tft)?reg->ReadString("ProgramX"):reg->ReadString("Program");
}

void __fastcall TForm1::Button2Click(TObject *Sender)
{
        HWND w3 = FindWindow("Warcraft III",0);
        if(w3)
        {
                SetActiveWindow(w3);
                ShowWindow(w3,SW_MAXIMIZE);
                return;        
        }
        AnsiString path= getW3Exe((TButton*)Sender==Button2);
        if(path.AnsiCompare("err")==0)
        {
                return;
        }
        ShellExecute(0,0,path.c_str(),0,(ExtractFileDir(path)).c_str(),SW_SHOWDEFAULT);
        Button1Click(0);
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

void __fastcall TForm1::Button4Click(TObject *Sender)
{
        Button8Click(0);        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button8Click(TObject *Sender)
{
        if(!inv_hks_on)
        {
                for(int i=2;i<8;++i)
                {
                        HotKeyClass * hkc = new HotKeyClass(inv_hks[i-2]);
                        KeyStruct * ks = new KeyStruct;
                        hkc->getKeyStruct(*ks);
                        if(!RegisterHotKey(Handle, i, ks->mods, ks->vk))
                        {
                                AnsiString errText = "Hotkey: ";
                                errText = errText+ks->vk;
                                errText += ErrHotkeyDefined2[lng];
                                ShowMessage(errText);
                        }

                }
                

        }
        if(inv_hks_on)
        {
                for(int i=2;i<8;++i)
                {
                        UnregisterHotKey(Handle, i);
                }
                
        }
        for(int i=0;i<inv_hks.size();++i)
        {
                inv_hks[i]->Enabled=inv_hks_on;
        }
        inv_hks_on=!inv_hks_on;

        Toggle(1,inv_hks_on);

}                                     
//---------------------------------------------------------------------------

void __fastcall TForm1::Button10Click(TObject *Sender)
{
        if(!tast_on)
                Button7Click(0);
        if(!inv_hks_on)
                Button8Click(0);
        if(!uon)
                Button13Click(0);

}
//---------------------------------------------------------------------------

void __fastcall TForm1::pingSockError(TObject *Sender,
      TCustomWinSocket *Socket, TErrorEvent ErrorEvent, int &ErrorCode)
{
        AnsiString err;
        switch(ErrorEvent)
        {
                case eeGeneral:
                {
                        err=ConError[lng];
                }
                case eeSend:
                {
                        err=ErrPing[lng];

                }
                default: err=ConError[lng];
        }
        ShowMessage(err);
        ErrorCode=0;

}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button11Click(TObject *Sender)
{
        pingSock->Host=hosts[ComboBox1->ItemIndex];
        pingSock->Active=true;
        pingSock->Socket->SendText("c");
        d=::GetTickCount();
}
//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------

void __fastcall TForm1::pingSockRead(TObject *Sender,
      TCustomWinSocket *Socket)
{
        Edit1->Text=IntToStr(::GetTickCount()-d)+"ms";
        pingSock->Active=false;
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------


bool __fastcall TForm1::SaveToFile(AnsiString fn)
{
        FileClose(FileCreate(fn));
        TFileStream* f= new TFileStream(fn, fmOpenWrite);
        LWTData ld;
        ld.cbs[0]=CheckBox4->Checked;
        ld.cbs[1]=CheckBox1->Checked;
        ld.cbs[2]=CheckBox2->Checked;
        ld.cbs[3]=CheckBox5->Checked;
        ld.cbs[4]=CheckBox6->Checked;
        ld.lng=lng;
        
        for(int i=0;i<2;++i)
        {
                ld.hks[i] = hks[i]->HotKey;
        }
        for(int i=2;i<8;++i)
        {
                ld.hks[i] = inv_hks[i-2]->HotKey;
        }
        ld.hks[8]=HotKey9->HotKey;
        ld.hks[9]=HotKey10->HotKey;
        f->Write(&ld, sizeof(ld));
        delete f;
        return true;
}
bool __fastcall TForm1::LoadFromFile(AnsiString fn)
{
        TFileStream* f= new TFileStream(fn, fmOpenRead);
        LWTData ld;
        f->Read(&ld, sizeof(LWTData));
        CheckBox4->Checked=ld.cbs[0];
        CheckBox1->Checked=ld.cbs[1];
        CheckBox2->Checked=ld.cbs[2];
        CheckBox5->Checked=ld.cbs[3];
        CheckBox6->Checked=ld.cbs[4];
        lng=ld.lng;
        ComboBox2->ItemIndex=lng;
        SetLanguage(lng);
        for(int i=0;i<2;++i)
        {
                hks[i]->HotKey=ld.hks[i];
        }
        for(int i=2;i<8;++i)
        {
                inv_hks[i-2]->HotKey=ld.hks[i];
        }
        HotKey9->HotKey=ld.hks[8];
        HotKey10Change(HotKey9);
        HotKey10->HotKey=ld.hks[9];
        HotKey10Change(HotKey10);
        delete f;
        return true;
}
void __fastcall TForm1::FormCloseQuery(TObject *Sender, bool &CanClose)
{
        for(unsigned int i=0;i<users.size();++i)
        {
                users[i].on=false;
        }
        TerminateThread(hThread,0);
        if(uon) Button16Click(0);
        SaveToFile(fn);
        SaveUserList(ul_fn);
        uon=true;
        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ListBox1Click(TObject *Sender)
{
                //TODO: user infos anzeigen

        int si = ListBox1->ItemIndex;
        if(si<0) si=0;
        //ShowMessage(IntToStr(users[si].on));
        Label20->Caption=users[si].name;
        Label18->Caption=gwNames[users[si].gateway];
        Label15->Caption="N/A";
        RichEdit1->Text="N/A";
        //if(lsocks[users[si].gateway]->Connected())
        ShowUserStatus(si);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button12Click(TObject *Sender)
{
        Form2->Show();        
}
//---------------------------------------------------------------------------
void __fastcall TForm1::AddUser(User u)
{
        for(unsigned int i=0;i<users.size();++i)
        {
                if((AnsiString(users[i].name)==u.name)&&(users[i].gateway==u.gateway))
                {
                        ShowMessage(ErrUserAlreadAdded[lng]);
                        return;
                }
        }
        users.push_back(u);
        ListBox1->AddItem(u.name, 0);
}


void __fastcall TForm1::ProcessUserStatus(UserStatus us,int nr,bool list)
{
        uss.push_back(us);
        //uls.push_back(uss.size()-1);
        users[nr].us=uss.size()-1;



}
/*
M�gliche antworten
public channel:
1018 INFO "ulak@Northrend is using Warcraft III The Frozen Throne
 in the channel Frozen Throne DEU-7."
private channel:
1018 INFO "Morrgoth@Northrend is using Warcraft III The Frozen
 Throne in a private channel."
 game:
 1018 INFO "Morrgoth@Northrend is using Warcraft III The Frozen Throne in game dota 5.84 download(host afk)."
*/




void __fastcall TForm1::Button15Click(TObject *Sender)
{
        if(OpenDialog1->Execute())
        {
                Edit2->Text=OpenDialog1->FileName;
        }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button14Click(TObject *Sender)
{
        TRegistry * reg=new TRegistry();
        reg->RootKey=HKEY_CURRENT_USER;
        reg->OpenKey("\\Software\\Blizzard Entertainment\\Warcraft III\\",true);
        reg->WriteString("InstallPath",ExtractFileDir(Edit2->Text));
        reg->WriteString("InstallPathX",ExtractFileDir(Edit2->Text));
        reg->WriteString("Program",(Edit2->Text));
        reg->WriteString("ProgramX",ExtractFileDir(Edit2->Text)+"\\Frozen Throne.exe");
        reg->CloseKey();
        ShowMessage(RegSuccess[lng]);

}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormClose(TObject *Sender, TCloseAction &Action)
{
        tray->IconVisible=false;
        delete tray;        
}
//---------------------------------------------------------------------------


void __fastcall TForm1::OnTrayMouseUp(TObject *Sender, TMouseButton Button,
      TShiftState Shift, int X, int Y)
{
        if(Button==mbRight)
        {
                TPoint p;
                GetCursorPos(&p);
                PopupMenu1->Popup(p.x,p.y);
        }
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//---------------------------------------------------------------


//---------------------------------------------------------------------------
void __fastcall TForm1::SaveUserList(AnsiString fn)
{
        FileClose(FileCreate(fn+EXT_CNT));
        FileClose(FileCreate(fn+EXT_UL));
        TFileStream * cntf = new TFileStream(fn+EXT_CNT,fmOpenWrite);
        int cnt = users.size();
        int * pcnt=&cnt;
        cntf->Write(pcnt,sizeof(cnt));
        delete cntf;
        TFileStream * ulf= new TFileStream(fn+EXT_UL,fmOpenWrite);
        for(int i=0;i<cnt;++i)
        {
                ulf->Write(&(users[i]),sizeof(User));
        }
        delete ulf;
}
void __fastcall TForm1::LoadUserList(AnsiString fn)
{
        if((!FileExists(fn+EXT_CNT))||(!FileExists(fn+EXT_UL)))
                return;
        TFileStream* cntf = new TFileStream(fn+EXT_CNT,fmOpenRead);
        int cnt=0;
        TFileStream* ulf = new TFileStream(fn+EXT_UL,fmOpenRead);
        cntf->Read(&cnt, sizeof(int));
        for(int i=0;i<cnt;++i)
        {
                User u;
                ulf->Read(&u, sizeof(User));
                AddUser(u);
        }
        delete ulf;
        delete cntf;
}
void __fastcall TForm1::DeleteUser(int nr)
{
        ListBox1->DeleteSelected();
        std::vector<User> tusers;
        for(int i=0;i<users.size();++i)
        {
                if(i!=nr)
                {
                        tusers.push_back(users[i]);
                }
        }
        users.clear();
        for(int i=0;i<tusers.size();++i)
        {
                users.push_back(tusers[i]);
        }

}
//---------------------------------------------------------------------------
UserStatus __fastcall TForm1::IsUserOn(AnsiString resp,int nr)
{
        UserStatus us;
        us.on=false;
        us.ort="N/A";
        if(resp.Length()<3)
        {
                us.on=false;
                us.ort="Error";
        }
        if(resp.Pos("1019 ERROR")>0)
        {
                users[nr].on=us.on=false;
                
                us.ort="N/A";
        }
        if(resp.Pos("1018 INFO")>0)
        {
                
                users[nr].on=us.on=true;
                //*******Ort herausfinden*******
                //*****�ffentlicher channel?****
                int channel=resp.Pos("channel");
                if((channel>0)&&(!resp.Pos("private")>0))
                {
                        //ShowMessage(resp);
                        us.ort=PubChan[lng];
                        us.ort+=resp.SubString(channel+8,resp.Length()-channel-9);
                }
                //*********privater channel?********************
                if((channel>0)&&(resp.Pos("private")>0))
                {
                        us.ort=PrivChan[lng];
                }
                //********privates game?***********************
                if((resp.Pos("private game")>0)&&(channel<=0))
                {
                        us.ort=PrivGame[lng];
                }
                //*********�ffentliches game?****************
                int game=resp.Pos("game");
                if((game>0)&&(resp.Pos("private game")<=0))
                {
                        us.ort=PubGame[lng];
                        us.ort+=resp.SubString(game+5,resp.Length()-game-6);
                }
        }
        return us;
}

//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

void __fastcall TForm1::Button13Click(TObject *Sender)
{
        if(!uon)
        {
                bool gs[4]={false,false,false,false};
                for(int i=0;i<4;++i)
                {

                        lsocks[i]->Connect();
                }
                
                hThread=CreateThread(0,0,tWrapper,0,0,0);
                Button12->Enabled=false;
                Button5->Enabled=false;

                Timer2->Enabled=true;
        }
        if(uon)
        {
                //TODO: userlist deaktivieren
                Button12->Enabled=true;
                Button5->Enabled=true;
                for(int i=0;i<4;++i)
                {
                        if(lsocks[i]->Connected())
                                lsocks[i]->Disconnect();
                }
                //ResetList();
                for(int i=0;i<users.size();++i)
                {
                        ShowUserStatus(i,false);
                        ListBox1->Repaint();
                }
                Timer2->Enabled=false;
        }
        uon=!uon;
        Toggle(2, uon);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ListBox1DblClick(TObject *Sender)
{
        int si=ListBox1->ItemIndex;
        Form3->ShowProfile(users[si].name, users[si].gateway);
        Form3->Show();           
        Form3->BringToFront();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ListBox1DrawItem(TWinControl *Control, int Index,
      TRect &Rect, TOwnerDrawState State)
{
        ListBox1->Canvas->Brush->Color=(users[Index].on)?MyGreen:MyRed;
        ListBox1->Canvas->Brush->Style=bsSolid;
        ListBox1->Canvas->TextRect(Rect,Rect.Left, Rect.Top,ListBox1->Items->Strings[Index]);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button16Click(TObject *Sender)
{
        Button13Click(0);
}
//----------------------------------------------------------------------------
void __fastcall TForm1::RefreshList(bool msg)
{
        std::vector<User> tmpUsers;
        std::vector<UserStatus> tmpUss(uss);
        for(int i=0;i<users.size();++i)
        {
                tmpUsers.push_back(users[i]);
        }
        for(int i=0;i<uss.size();++i)
        {
                tmpUss.push_back(uss[i]);
        }
        for(unsigned int i=0;i<users.size();++i)
        {
                while(!lsocks[users[i].gateway]->Connected());
                UserStatus us=getUserStatus(i);
                ProcessUserStatus(us, i, true);
                ShowUserStatus(i,false);
                ListBox1->Repaint();
        }
        if(msg)
        {
                AnsiString msg("");
                for(int i=0;i<users.size();++i)
                {

                        if(tmpUsers[i].on!=users[i].on)
                        {
                                if(lng==0)
                                {
                                        msg=WideString(users[i].name)+" hat das Battle.net "+((users[i].on)?"betreten":"verlassen");
                                        tray->ShowBalloonHint("LWT - Userliste",msg,bitInfo,20);
                                }
                                if(lng==1)
                                {
                                        msg=WideString(users[i].name)+" has "+((users[i].on)?"entered":"left")+" the Battle.net";
                                        tray->ShowBalloonHint("LWT - Userliste",msg,bitInfo,20);
                                }
                                goto end_for;
                        }
                        if(tmpUss[tmpUsers[i].us].ort.AnsiCompareIC(uss[users[i].us].ort)!=0)
                        {
                                AnsiString resp = uss[users[i].us].ort;
                                AnsiString name = WideString(users[i].name);
                                int game = resp.Pos(PrivGame[lng]);
                                if(game>0)
                                {
                                        msg=name+UlPrivateGame[lng];
                                }
                                game=resp.Pos(PubGame[lng]);
                                if(game>0)
                                {
                                        if(lng==0)
                                        {
                                                int l=WideString(PubGame[lng]).Length();
                                                msg=name+" hat das �ffentliche Spiel: \"";
                                                msg+=resp.SubString(game+l,resp.Length()-game-l)+"\" betreten";
                                        }
                                        if(lng==1)
                                        {
                                                int l=WideString(PubGame[lng]).Length();
                                                msg=name+" has entered the public game: \"";
                                                msg+=resp.SubString(game+l,resp.Length()-game-l)+"\"";
                                        }

                                }
                                if(msg!="")
                                tray->ShowBalloonHint("LWT - Userlist",msg,bitInfo,20);
                        }
                        end_for:
                }
        }
}

DWORD WINAPI tWrapper(LPVOID p)
{
        bool b=false;
        if((int)p==1) b=true;
        Form1->RefreshList(b);
}
UserStatus __fastcall TForm1::getUserStatus(int nr)
{
        UserStatus us;
        int gw = users[nr].gateway;
        us.on=false;
        us.ort="N/A";
        if(!(lsocks[gw]->Connected()))
        {
                ShowMessage("NOT CONNECTED");
                return us;

        }
        try
        {
        AnsiString cmd = "/whois "+users[nr].name+"@"+gwPres[gw]+"\r\n";
        lsocks[gw]->Write(cmd);
        AnsiString r="";
        do
        {
                r=lsocks[gw]->ReadLn();
                bool empty=r.IsEmpty();
                int info=r.Pos("1018 INFO"),
                    error=r.Pos("1019 ERROR");
                if((!empty)&&((info>0)||(error>0)))
                {
                        us=IsUserOn(r, nr);
                        ::Sleep(1000);
                        return us;
                }
        }
        while(((!r.Pos("1019 ERROR")>=0)&&(!r.Pos("1018 INFO")>=0))||(r.IsEmpty()));
        }
        catch(Exception& e)
        {
                //ShowMessage("Verbindungsfehler");
        }
        return us;
}
void __fastcall TForm1::lsockConnected(TObject *Sender)
{
        lsocks[((TIdTCPClient*)Sender)->Tag]->Write("c");
        lsocks[((TIdTCPClient*)Sender)->Tag]->Write("anonymous\r\n");
        AnsiString r="";
        list_on=true;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ShowUserStatus(int nr, bool labs)
{
        if(!list_on) return;

        if(users[nr].us>uss.size()) return;
        if(labs)
        {
                
                RichEdit1->Text=uss[users[nr].us].ort;
                Label15->Caption=(uss[users[nr].us].on)?"Online":"Offline";
        }
        TOwnerDrawState odw;
        odw.Clear();
        TRect r;
        r.Top=(nr)*13;
        r.Left=0;
        r.Bottom=(nr)*13;
        r.Right=117;
        ListBox1DrawItem(ListBox1,nr, r,odw);
        SendMessage(ListBox1->Handle,WM_PAINT,0,0);
        Application->ProcessMessages();
}

void __fastcall TForm1::Timer2Timer(TObject *Sender)
{
        CreateThread(0,0,tWrapper,(void*)1,0,0);
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

void __fastcall TForm1::killTimerTimer(TObject *Sender)
{
        Close();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ResetList()
{
        for(unsigned int i=0;i<users.size();++i)
        {
                users[i].on=false;
                uss[users[i].us].on=false;
                uss[users[i].us].ort="N/A";
        }
}
void __fastcall TForm1::msgTimerTimer(TObject *Sender)
{
        msgTimer->Enabled=false;
        ShowMessage(ConError[lng]);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::SetLanguage(int nr)
{
        TabSheet1->Caption=Page1[nr];
        TabSheet2->Caption=Page2[nr];
        TabSheet3->Caption=Page3[nr];
        Label2->Caption=Lab2[nr];
        Label3->Caption=Lab3[nr];
        Label24->Caption=Lab24[nr];
        Button10->Caption=But10[nr];
        GroupBox2->Caption=GB2[nr];
        CheckBox4->Caption=CB4[nr];
        CheckBox2->Caption=CB2[nr];
        Label6->Caption=Lab6[nr];
        Label7->Caption=Lab7[nr];
        GroupBox3->Caption=GB3[nr];
        //Label26->Caption=Lab26[nr];
        Label16->Caption=Lab16[nr];
        Button12->Caption=But12[nr];
        Button5->Caption=But5[nr];
        Label23->Caption=Lab23[nr];
        Label21->Caption=Lab21[nr];
        Button14->Caption=But14[nr];
        Button6->Caption=(auto_en)?But6Act[nr]:But6InAct[nr];
        Button1->Caption=But1[nr];
        Label28->Caption=Lab28[nr];
        Label29->Caption=Lab28[nr];
        GroupBox1->Caption=GB1[nr];
        Button2->Caption=But2[nr];
        Button3->Caption=But3[nr];
        Button18->Caption=But18[nr];
        auto_en=AutoStart::AutoStartEnabled(KeyName);
        Button6->Caption=(auto_en)?But6InAct[nr]:But6Act[nr];
        Anzeigen1->Caption=CapShow[nr];
        Beenden1->Caption=CapClose[nr];

        
        Toggle(0,tast_on);
        Toggle(1,inv_hks_on);
        Toggle(2,uon);
}
void __fastcall TForm1::ComboBox2Change(TObject *Sender)
{
        lng=ComboBox2->ItemIndex;
        SetLanguage(lng);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::HotKey10Change(TObject *Sender)
{
        THotKey* sel = fhks[((THotKey*)Sender)->Tag];
        HotKeyClass hkc(sel);
        KeyStruct ks;
        hkc.getKeyStruct(ks);
        UnregisterHotKey(Handle, ((THotKey*)Sender)->Tag+8);
        if(!RegisterHotKey(Handle, ((THotKey*)Sender)->Tag+8,ks.mods,ks.vk))
        {
                ShowMessage("Hotkey: "+((char)ks.vk)+ErrHotkeyDefined2[lng]);
                return;
        }
        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button17Click(TObject *Sender)
{
        Form4->Show();        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button18Click(TObject *Sender)
{
        if(tast_on)
                Button7Click(0);
        if(inv_hks_on)
                Button8Click(0);
        if(uon)
                Button13Click(0);

}
//---------------------------------------------------------------------------

DWORD WINAPI Blend(LPVOID)
{
        Form1->BlendForm();
}


//---------------------------------------------------------------------------
void __fastcall TForm1::BlendForm()
{
        for(int i=0;i<17;++i)
        {
                AlphaBlendValue+=15;
                ::Sleep(27);
        }
        Repaint();
}
bool __fastcall TForm1::FormHelp(WORD Command, int Data, bool &CallHelp)
{
        Button17Click(0);        
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

void __fastcall TForm1::minTimerTimer(TObject *Sender)
{
        Button1Click(0);
        minTimer->Enabled=false;        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::StartW3ROC1Click(TObject *Sender)
{
        Button2Click(Button3);        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::StartW3TFT1Click(TObject *Sender)
{
        Button2Click(Button2);        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ComboBox2KeyPress(TObject *Sender, char &Key)
{
        Key=0;        
}
//---------------------------------------------------------------------------

