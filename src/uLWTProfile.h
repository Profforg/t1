//---------------------------------------------------------------------------

#ifndef uLWTProfileH
#define uLWTProfileH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "SHDocVw_OCX.h"
#include "gHeader.h"
#include <OleCtrls.hpp>
//---------------------------------------------------------------------------
class TForm3 : public TForm
{
__published:	// Von der IDE verwaltete Komponenten
        TCppWebBrowser *CppWebBrowser1;
private:	// Anwender-Deklarationen
public:		// Anwender-Deklarationen
        __fastcall TForm3(TComponent* Owner);
        void __fastcall ShowProfile(AnsiString name, int gw);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm3 *Form3;
//---------------------------------------------------------------------------
#endif
