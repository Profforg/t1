//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include "uLwt.h"
//---------------------------------------------------------------------------
USEFORM("uLwt.cpp", Form1);
USEFORM("uLWT_newUser.cpp", Form2);
USEFORM("uLWTProfile.cpp", Form3);
USEFORM("uLWTAbout.cpp", Form4);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR, int)
{
        try
        {
                
                 Application->Initialize();
                 Application->CreateForm(__classid(TForm1), &Form1);
                 Application->CreateForm(__classid(TForm2), &Form2);
                 Application->CreateForm(__classid(TForm3), &Form3);
                 Application->CreateForm(__classid(TForm4), &Form4);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        catch (...)
        {
                 try
                 {
                         throw Exception("");
                 }
                 catch (Exception &exception)
                 {
                         Application->ShowException(&exception);
                 }
        }
        return 0;
}
//---------------------------------------------------------------------------
