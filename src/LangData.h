//---------------------------------------------------------------------------

#ifndef LangDataH
#define LangDataH
#include <SysUtils.hpp>
//---------------------------------------------------------------------------
const int cnt=2;
const AnsiString Page1[cnt]={"Hauptmen�","Main-Menu"},
Page2[cnt]={"Tastatur","Keyboard"},
Page3[cnt]={"Inventar-Hotkeys","Inventory-Hotkeys"},
//Page4[cnt]={"Battle.net","Battle.net"},
Lab2[cnt]={"Tastatur-Optionen", "Keyboard-Options"},
Lab3[cnt]={"Inventar-Hotkeys","Inventory-Hotkeys"},
Lab24[cnt]={"Battle.net-Userliste","Battle.net-Userlist"},
LabAct[cnt]={"Aktiviert","active"},
LabInAct[cnt]={"Inaktiv","inactive"},
ButAct[cnt]={"Aktivieren","Activate"},
ButInAct[cnt]={"Deaktivieren","Deactivate"},
But10[cnt]={"Alle aktivieren","Activate all"},
But18[cnt]={"Alle deaktivieren","Deactivate all"},
GB2[cnt]={"Energie-Balken anzeigen","Show energy"},
Lab6[cnt]={"Eigene HP","Own HP"},
Lab7[cnt]={"Gegnerische HP","Hostile HP"},
CB4[cnt]={"Aktiviert","Activated"},
CB2[cnt]={"Windows-Tasten","Windows-Keys"},
GB3[cnt]={"Inventar","Inventory"},
Lab26[cnt]={"Zum An/Ausschalten der Inventar-Hotkeys \"Bild-hoch\" dr�cken!",
            "To switch the hotkeys on/off press \"Page-up\"!"},
Lab16[cnt]={"Ort:","Location:"},
But12[cnt]={"User hinzuf�gen","Add user"},
But5[cnt]={"User l�schen","Delete user"},
Lab23[cnt]={"Doppelklick auf Namen f�r Profil!","Double-Click on a name to show the profile!"},
Lab21[cnt]={"Deine Warcraft 3 Registry Eintr�ge sind durch Windows-Reinstall falsch/nicht vorhanden?\nDann benutze dieses Men� um die Eintr�ge wiederherzustellen",
            "Your Wacraft 3 registry entries are invalid or not existing because you reinstalled Windows?\nThen use this tool to set the required entries"},
But14[cnt]={"Eintragen", "Make entries"},
But6Act[cnt]={"Autostart ein","Autostart on"},
But6InAct[cnt]={"Autostart aus","Autostart off"},
But1[cnt]={"Minimieren","Minimize"},
But2[cnt]={"Warcraft TFT starten","Start Warcraft TFT"},
But3[cnt]={"Warcraft ROC starten","Start Warcraft ROC"},
F2But1[cnt]={"Hinzuf�gen","Add"},
F2But2[cnt]={"Abbrechen","Cancel"},
ConError[cnt]={"Verbindungsfehler","Connection error"},
DllMissing[cnt]={" nicht gefunden"," not found"},
ErrHotkeyDefined[cnt]={"Hotkey schon belegt!","Hotkey already defined"},
ErrDirNotFound[cnt]={"Ihr Warcraft-Verzeichnis konnte nicht gefunden werden\n�berpr�fen sie ihre Registry-Eintr�ge","Your Warcraft directory could not be found.\nCheck your registry entries"},
ErrHotkeyDefined2[cnt]={" konnte nicht registriert werden\nEventuell schon von anderer Anwendung belegt"," could not be registered\nPerhaps already defined by an other application!"},
ErrPing[cnt]={"Der Server konnte nicht angepingt werden","Ping could not be sent to the server"},
RegSuccess[cnt]={"Registrierungsdaten eingetragen","Registry entries created"},
UlPrivateGame[cnt]={" hat ein privates Spiel betreten"," has entered a private game"},
GB1[cnt]={"Tasten deaktivieren","Deactivate keys"},
ErrUserAlreadAdded[cnt]={"User schon in der Liste","User already added to the list"},
PubChan[cnt]={"�ffentlicher Channel:\n","Public channel:\n"},
PrivChan[cnt]={"Privater Channel\n","Private channel\n"},
PrivGame[cnt]={"Privates Spiel","Private game"},
CapClose[cnt]={"Beenden","Exit"},
CapShow[cnt]={"Anzeigen","Show"},
PubGame[cnt]={"�ffentliches Spiel:\n","Public game:\n"},
Lab28[cnt]={"Hotkey zum An/Ausschalten", "Hotkey to swith on/off"};
//But2[cnt]={"Wacraft TFT starten","Start Warcraft TFT"},
//But3[cnt]={"Wacraft ROC starten","Start Warcraft ROC"};
#endif
