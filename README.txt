Lescos Warcraft Toolkit - README
<------------------------------------------->
1. Features descripted
2. FAQ
<------------------------------------------->
1.1 Keyboard options
	-----------------------------
	Show energy:
	Here you can set hotkeys to show the units energy permanently in game
	Usage: Set the hotkeys.
	       Activate the keyboard options
	       Start a game.
	       Press the defined hotkeys to switch it on/off.
	-----------------------------
	Deactivate keys:
	Here you can deactivate keys that could disturb you while game.
	Usage: Select the keys to disable.
	       Activate the keyboard options.
	      
1.2 Inventory-Hotkeys:
        --------------------------------
        Here you can define hotkeys for the inventory slots of your hero/units.
        Usage: Set the hokeys.
               Activate the Inventory-Hotkeys.
       
1.3 Battle.net:
	---------------------------------
	Battle.net Userlist:
	Here you can monitor Battle.net users.
	Usage: Add the users, you want to monitor to the list.
	       Activate the list.
	----------------------------------
	Ping:
	Determines your ping.
	Usage: Select a gateway and press the "Ping" button.

1.4 Registry:
	----------------------------------
	If you need the registry entries to patch your Warcraft 3, this feature helps you.
	Usage: Enter the path of your "Warcraft III.exe"
	       Klick "Make entries".
	       

2.0 Q:Why doesn't LWT start?
    A:A possible reason can be old data file of a previous version.
      Delete all the files in the directory and extract the files to the directory again.
    Q:Is the source of LWT available?
    A:Yes, it is stored in the folder "./src" of the toolkit.
    Q:Can I submit feedback?
    A:Yes, there are several possibilities to do that:
      - Email me to: lesco@gmx.de
      - Post int the forum on www.lesco-warcraft.de.vu
      - Post in the specific thread on the inwarcraft.de forum.
    

	   
	     
	       
	
